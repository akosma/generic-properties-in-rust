use active_record::object::{ActiveRecord, Object};

fn main() {
    let mut o = Object::create();
    o.set_name("Toto");
    o.set_price(1.23);
    o.set_valid(true);
    o.save();

    o.set_valid(false);
    o.save();
    println!("{}", o);
    let _b = Object::from_database("test", 13);
}
